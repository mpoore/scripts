#!/bin/bash

# Installs JFrog Artifactory OSS on Centos OS
# @author Mark Brookfield <>
# @modified Michael Poore

# Fetch Artifactory RPM
curl -sL https://bintray.com/jfrog/artifactory-rpms/rpm | sudo tee /etc/yum.repos.d/bintray-jfrog-artifactory-rpms.repo

# Install Artifactory
if ! rpm -qa | grep artifactory; then
    /usr/bin/yum install -y jfrog-artifactory-oss
fi

# Create ARTIFACTORY_HOME environment variable
echo "export ARTIFACTORY_HOME=/opt/jfrog/artifactory" >> /etc/profile.d/sh.local

# Read environment variables
source /etc/profile

# Open firewall port (default is 8082 from v7.x)
firewall-cmd --permanent --add-port=8082/tcp
firewall-cmd --reload

# Start the service
systemctl start artifactory.service

# Install JFrog CLI
curl -fL https://getcli.jfrog.io | sh
mv jfrog /usr/local/bin/