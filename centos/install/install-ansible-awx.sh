#!/bin/bash
# Install AWX for Ansible
# @author Michael Poore

# Configure SElinux
yum -y install policycoreutils-python
semanage port -a -t http_port_t -p tcp 8050
semanage port -a -t http_port_t -p tcp 8051
semanage port -a -t http_port_t -p tcp 8052
setsebool -P httpd_can_network_connect 1

# Disable firewall
systemctl stop firewalld
systemctl disable firewalld

# Configure repositories
# Activate EPEL release
yum -y install epel-release
# Software collections
yum -y install centos-release-scl centos-release-scl-rh
# AWX
yum install -y wget
wget -O /etc/yum.repos.d/ansible-awx.repo https://copr.fedorainfracloud.org/coprs/mrmeee/ansible-awx/repo/epel-7/mrmeee-ansible-awx-epel-7.repo
# RabbitMQ and Erlang
echo "[bintraybintray-rabbitmq-rpm] 
name=bintray-rabbitmq-rpm 
baseurl=https://dl.bintray.com/rabbitmq/rpm/rabbitmq-server/v3.7.x/el/7/
gpgcheck=0 
repo_gpgcheck=0 
enabled=1" > /etc/yum.repos.d/rabbitmq.repo
echo "[bintraybintray-rabbitmq-erlang-rpm] 
name=bintray-rabbitmq-erlang-rpm 
baseurl=https://dl.bintray.com/rabbitmq-erlang/rpm/erlang/21/el/7/
gpgcheck=0 
repo_gpgcheck=0 
enabled=1" > /etc/yum.repos.d/rabbitmq-erlang.repo

# Installation
# RabbitMQ
yum -y install rabbitmq-server
# GIT
yum -y install rh-git29
# PostgreSQL and memcached
yum install -y rh-postgresql10 memcached
# NGINX
yum -y install nginx
# Python dependecies
yum -y install rh-python36
yum -y install --disablerepo='*' --enablerepo='copr:copr.fedorainfracloud.org:mrmeee:ansible-awx, base' -x *-debuginfo rh-python36*
# AWX-RPM
yum install -y ansible-awx

# Configuration
# Initialize DB
scl enable rh-postgresql10 "postgresql-setup initdb"
# Start Postgresql
systemctl start rh-postgresql10-postgresql.service
# Start RabbitMQ
systemctl start rabbitmq-server
# Create Postgres user and DB
scl enable rh-postgresql10 "su postgres -c \"createuser -S awx\""
scl enable rh-postgresql10 "su postgres -c \"createdb -O awx awx\""
# Import data
sudo -u awx scl enable rh-python36 rh-postgresql10 rh-git29 "GIT_PYTHON_REFRESH=quiet awx-manage migrate"
# Configuration AWX
echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'root@localhost', 'password')" | sudo -u awx scl enable rh-python36 rh-postgresql10 "GIT_PYTHON_REFRESH=quiet awx-manage shell"
sudo -u awx scl enable rh-python36 rh-postgresql10 rh-git29 "GIT_PYTHON_REFRESH=quiet awx-manage create_preload_data" # Optional Sample Configuration
sudo -u awx scl enable rh-python36 rh-postgresql10 rh-git29 "GIT_PYTHON_REFRESH=quiet awx-manage provision_instance --hostname=$(hostname)"
sudo -u awx scl enable rh-python36 rh-postgresql10 rh-git29 "GIT_PYTHON_REFRESH=quiet awx-manage register_queue --queuename=tower --hostnames=$(hostname)"
# Configure NGINX proxy
wget -O /etc/nginx/nginx.conf https://raw.githubusercontent.com/MrMEEE/awx-build/master/nginx.conf

# Start AWX
systemctl start awx
systemctl enable awx