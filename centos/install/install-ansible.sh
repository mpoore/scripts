#!/bin/bash

# Install vanilla Ansible
# @author Michael Poore
# @website https://blog.v12n.io

# Install EPEL repository
echo "Installing EPEL repository"
sudo yum install -y -q epel-release

# Install Ansible
echo "Installing Ansible"
sudo yum install -y -q ansible