#!/bin/bash

# Installs Java OpenJDK 1.8.0 on Centos OS
# @author Mark Brookfield <>

# Install Java OpenJDK
if ! rpm -qa | grep openjdk; then
    /usr/bin/yum install -y java-1.8.0-openjdk-devel
fi

# Add JAVA_HOME environment variable
echo "export JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which javac)))))" >> /etc/profile.d/sh.local

# Read environment variables
source /etc/profile

# Add to path 
echo "export PATH=$PATH:$JAVA_HOME/bin" >> /etc/profile.d/sh.local

# Read environment variables, again
source /etc/profile