#!/bin/bash
# Prepare Centos OS template for vSphere cloning
# @author Michael Poore

# Update existing packages
/usr/bin/yum update --skip-broken -y

# Add additional packages
/usr/bin/yum install -y cloud-utils-growpart perl wget git yum-utils open-vm-tools
#/usr/bin/yum install -y cloud-init

# Stop logging and auditing services
/sbin/service rsyslog stop
/sbin/service auditd stop

# Disable firewalld
#systemctl disable firewalld

# Configure cloud-init
#echo "network: {config: disabled}" > /etc/cloud/cloud.cfg.d/99-custom-networking.cfg
#cloud-init clean

# Clean up packages and kernels
/bin/package-cleanup --oldkernels --count=1
/usr/bin/yum clean all

# Rotate logs and cleanup
/usr/sbin/logrotate –f /etc/logrotate.conf
/bin/rm –f /var/log/*-???????? /var/log/*.gz
/bin/rm -f /var/log/dmesg.old
/bin/rm -rf /var/log/anaconda
/bin/cat /dev/null > /var/log/audit/audit.log
/bin/cat /dev/null > /var/log/wtmp
/bin/cat /dev/null > /var/log/lastlog
/bin/cat /dev/null > /var/log/grubby

# Remove network interface config files
/bin/rm -f /etc/udev/rules.d/70*
/bin/sed -i '/^(HWADDR|UUID)=/d' /etc/sysconfig/network-scripts/ifcfg-e*

# Tidy /tmp and history
/bin/rm –rf /tmp/*
/bin/rm –rf /var/tmp/*
/bin/rm –f /etc/ssh/*key*
/bin/rm -f ~root/.bash_history
unset HISTFILE
/bin/rm -rf ~root/.ssh/
/bin/rm -f ~root/anaconda-ks.cfg

# Shutdown etc
echo "Now run 'history –c'"
echo "Finally execute 'sys-unconfig'"