#!/bin/bash

# Join CentOS VM to AD Domain
# @author Michael Poore
# @website https://blog.v12n.io

# Set variables
realm="v12n.co"
user="svc_adjoin"
ou="OU=servers,OU=linux,OU=services,OU=v12n,DC=v12n,DC=co"
adminGroupDN="CN=gg_linux_admins,OU=groups,OU=v12n,DC=v12n,DC=co"
adminGroup="gg_linux_admins"

# Install package dependencies
echo "Installing package dependencies"
yum install -y -q realmd oddjob oddjob-mkhomedir sssd adcli samba-common-tools

# Discover realm
echo "Discovering realm $realm"
realm discover $realm

# Join realm
echo "Joining realm $realm"
realm join $realm --computer-ou=$ou -U $user

# Add extra configuration to /etc/sssd/sssd.conf
echo "Updating sssd.conf settings"
echo "cache_credentials = False" >> /etc/sssd/sssd.conf
echo "use_fully_qualified_names = True" >> /etc/sssd/sssd.conf
echo "fallback_homedir = /home/%u" >> /etc/sssd/sssd.conf
echo "ad_access_filter = (&(memberOf=$adminGroupDN))" >> /etc/sssd/sssd.conf

# Update sudoers file
echo "Adding $adminGroup to sudoers"
echo "%$adminGroup@$realm ALL=(ALL) ALL" >> /etc/sudoers

# Restart sssd
echo "Restarting sssd"
systemctl restart sssd