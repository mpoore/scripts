#!/bin/bash

# Grows root logical volume to maximum size for the disk on Centos OS
# @author Michael Poore

# Check for required packages and install
if ! rpm -qa | grep growpart; then
    /usr/bin/yum install -y cloud-utils-growpart
fi

# Get logical volume path for root
rootPath=$( df / | awk '!/Used/ {print $1}' )

# Get volume group name
vgName=$( lvs --select lv_name="root" | awk '!/LSize/ {print $2}' )

# Get physical volume name etc
pvName=$( pvdisplay --select vg_name="$vgName" | grep "PV Name" | awk '{print $3}' )
pvDevice=$( echo $pvName | grep -o '\/dev\/[a-z]*' )
partNum=$( echo $pvName | grep -o '[0-9]' )

# Extend root volume
growpart $pvDevice $partNum
pvresize $pvName
lvextend -l +100%FREE $rootPath
xfs_growfs /